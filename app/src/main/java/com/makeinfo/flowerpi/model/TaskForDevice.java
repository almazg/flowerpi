package com.makeinfo.flowerpi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by almaz on 09.09.15.
 */
public class TaskForDevice {

    @SerializedName("tasks-id")
    final Integer tasks_id;

    @SerializedName("device-id")
    final Integer device_id;
    final String action;

    public TaskForDevice(Integer tasks_id, Integer device_id, String action) {
        this.tasks_id = tasks_id;
        this.device_id = device_id;
        this.action = action;
    }
}
