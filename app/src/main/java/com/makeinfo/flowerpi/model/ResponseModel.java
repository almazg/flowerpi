package com.makeinfo.flowerpi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModel {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("task-id")
    @Expose
    private Integer taskId;

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The task id
     */
    public void setStatus(String status) { this.status = status; }

    /**
     *
     * @return
     * The taskId
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     *
     * @param taskId
     * The taskId
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }
}