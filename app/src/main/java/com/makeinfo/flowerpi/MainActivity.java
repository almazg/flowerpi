package com.makeinfo.flowerpi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.makeinfo.flowerpi.API.ISmartHouseApi;
import com.makeinfo.flowerpi.model.ResponseModel;
import com.makeinfo.flowerpi.model.TaskForDevice;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {

    Button click;
    TextView textView;
    ProgressBar progressBar;
    String API = "https://stark-island-1463.herokuapp.com/";

    public void onClickOff(View v) {
        Integer user = 1; //Тестовый юзер, норм все!
        progressBar.setVisibility(View.VISIBLE);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();
        ISmartHouseApi houseApi = restAdapter.create(ISmartHouseApi.class);

        Integer taskId = 1;
        Integer device_id = 1;
        String action = "off";

        houseApi.setNewTask(new TaskForDevice(taskId, device_id, action), user, new Callback<ResponseModel>() {
            @Override
            public void success(ResponseModel responseModel, Response response) {
                textView.setText("Response status :" + response.getStatus());
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void failure(RetrofitError error) {
                textView.setText(error.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        click = (Button) findViewById(R.id.button);
        textView = (TextView) findViewById(R.id.tv);
        progressBar = (ProgressBar) findViewById(R.id.pb);
        progressBar.setVisibility(View.INVISIBLE);


        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer user = 1; //Тестовый юзер, норм все!
                progressBar.setVisibility(View.VISIBLE);

                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(API).build();
                ISmartHouseApi houseApi = restAdapter.create(ISmartHouseApi.class);

                Integer taskId = 1;
                Integer device_id = 1;
                String action = "on";

                houseApi.setNewTask(new TaskForDevice(taskId, device_id, action), user, new Callback<ResponseModel>() {
                    @Override
                    public void success(ResponseModel responseModel, Response response) {
                        textView.setText("Response status :" + response.getStatus());
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        textView.setText(error.getMessage());
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
    }
}
