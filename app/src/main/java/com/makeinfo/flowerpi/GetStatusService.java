package com.makeinfo.flowerpi;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.makeinfo.flowerpi.API.ISmartHouseApi;
import com.makeinfo.flowerpi.model.ResponseModel;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetStatusService extends Service {

    final String LOG_TAG = "myLogs";
    final String API = "https://stark-island-1463.herokuapp.com/";
    final int NOTIFY_ID = 101;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        someTask();
        return super.onStartCommand(intent, flags, startId);
    }

    void someTask() {
            new Thread(new Runnable() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void run() {
                Integer user = 1; //Тестовый юзер, норм все!
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(API).build();
                ISmartHouseApi houseApi = restAdapter.create(ISmartHouseApi.class);

                Integer taskId = 1;

                while(true) {
                    houseApi.getStatus(user, taskId, new Callback<ResponseModel>() {
                        @Override
                        public void success(ResponseModel ResponseModel, Response response) {
                            if(ResponseModel.getStatus() == "success") {
                                Context context = getApplicationContext();

                                Intent notificationIntent = new Intent(context, MainActivity.class);
                                PendingIntent contentIntent = PendingIntent.getActivity(context,
                                        0, notificationIntent,
                                        PendingIntent.FLAG_CANCEL_CURRENT);

                                Resources res = context.getResources();

                                Notification.Builder builder = new Notification.Builder(context);
                                builder.setContentIntent(contentIntent)
                                        .setSmallIcon(R.mipmap.ic_launcher)
                                        .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                                        .setWhen(System.currentTimeMillis())
                                        .setAutoCancel(true);

                                Notification notification = builder.build();

                                NotificationManager notificationManager = (NotificationManager) context
                                        .getSystemService(Context.NOTIFICATION_SERVICE);
                                builder.setTicker("Device on!")
                                        .setContentTitle("device on!")
                                        .setContentText("DEVICE ON!");
                                notificationManager.notify(NOTIFY_ID, notification);
                            }
                            try {
                                TimeUnit.SECONDS.sleep(5);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            error.printStackTrace();
                            Log.d(LOG_TAG, "fail request");
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }
}
