package com.makeinfo.flowerpi.API;

import com.makeinfo.flowerpi.model.ResponseModel;
import com.makeinfo.flowerpi.model.TaskForDevice;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;


public interface ISmartHouseApi {
    @GET("/users/{user-id}/tasks/status")
    void getStatus(@Path("user-id") Integer userId, @Query("task-id") Integer taskId,
                          Callback<ResponseModel> response);

    @POST("/users/{user-id}/tasks/")
    void setNewTask(@Body TaskForDevice tasks, @Path("user-id") Integer userId,
                           Callback<ResponseModel> response);
}
